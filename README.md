general
--------------------------------------

use the following form for the functions(or structures/if/else/any thing wiht brace)

*
type funtionname ( x )
{

}

*
if ( x )
{

}
else
{

}

Every item should be separated by space

*
bad��if(x=12)
good: if ( x = 12 )


name
--------------------------------------

To avoid two value have the same name, name of the value should be at list three letters.(expect the value for circulation )

*
bad:int a, b
good:int statenumber, isEmpty;

As for circulation, use i,j,l,k,m,n...

*
for(int i=0;i<Max;i++)

If there are several elements in the name that make the use of the value or function clearly ,then use upper case:

*
SList
CName


use"_" to expand series of value name

*
 Battle_hp, Battle_mp


class
--------------------------------------
In the beginng of the class should describe the class and the possible use if it's special

*
/*
SList
single list used to store data
*/




comments
--------------------------------------
Every function should have a comment to claim the use of the funtion.

*
bool Comparetwostring(); // used for comparing two string. if they're same the return 1.else return 0

if there are several braches of the programme, claim it on the start with latter and number stand for every brach.

*
if()// if XXX then N1 else N2
{
}
else() //N2
{
}

In the key point of the progamme test, should have a extra comment;

*
StateNumber = *this;//StateNumber: 1 stand for beginning; 2 stand for ending; 3 stand for middle;
read me
 
